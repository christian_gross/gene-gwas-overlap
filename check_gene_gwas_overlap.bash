#!/bin/bash
${HOME}/anaconda3/bin/python ${HOME}/gene-gwas-overlap/check_gene_gwas_overlap.py "--gwas_sets" "${1}" "--outfile" "${2}" "--gene_file" "${3}" "--gwas_name" "${4}" "--build" "${5}" "--up" "${6}" "--down" "${7}" "--pvalue" "${8}"
