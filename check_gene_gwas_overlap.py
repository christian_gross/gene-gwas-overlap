#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 17 15:23:37 2021

@author: christiangross

This script will iterate over a selected number of genes and check if they are overlapping with a
selected number of GWAS data sets. The output is a multiindexed pandas dataframe with GWAS-Gene
pairs as indexes and lists of uniq variant identifiers as their single value. The output is
written to a single pickle file, which then can be queried to identify overlapping instruments.

###################################################################################################
TODO Implement maf filter subsequent to the pvalue filter.
TODO Refactor main()

DONE Add selection of target genes for GTEx files.
DONE implement portalocker to write to one final file.
DONE Build this script more like run_mr such that it reads in the gwas_sets and then selects the
    row it is supposed to run.
###################################################################################################
"""
import sys
import os
import argparse
import re
import pickle
import warnings
from ensembl_rest_client import lookup
from merit import merit_errors
from merit import dataset
import portalocker
import numpy as np
import pandas as pd
warnings.filterwarnings('ignore')

def main():
    """
    The main entry point for the script
    """

    ###  getting arguments
    #parser = _test_default_set_args()
    parser = set_args()
    args = parse_args(parser)

    #!!!function needs to be adjusted in case non human species should be queried in future
    server,species = _select_server_species(args.build)

    ids = retrieve_genes_from_file(args.gene_file)

    #retrieve gene locations
    try:
        rc = lookup.Lookup(ping=True,url=server)
        data = rc.post_lookup_id(ids,
                         data_format='full',
                         expand=False,
                         species=species)
        coordinates = [(x['seq_region_name'], x['start'],x['end'],x['id']) for x in data.values()]
    except ConnectionError:
        sys.exit('Terminating: Unable to access ensembl with the following server url\
                 :{}'.format(server))


    #retrieve gwas data sets that are supposed to be checked.
    gwas_sets_df = pd.read_csv(args.gwas_sets,sep='\t',index_col=0)

    #select GWAS for
    gwas_sets_df = select_gwas(gwas_sets_df,args.gwas_name,args.outfile)

    #this dict will store the uni_id of the overlapping instruments for each gene
    output_dict = {}

    for i,gwas_row in gwas_sets_df.iterrows():
        #Parse chrpos_spec
        if not (gwas_row.chrpos_spec == '' or pd.isna(gwas_row.chrpos_spec)):
            if bool(re.search(';', gwas_row.chrpos_spec)):
                gwas_row.chrpos_spec = [i.strip() for i in gwas_row.chrpos_spec.split(';')]
            else:
                raise IndexError('`chrpos_spec` should be a `;` delimited string')

        read_params = gwas_row.loc['effect_allele':'compression']

        #this is important to see which gwas may fail in case that the script crashes.
        for gene_coords in coordinates:
            try:
                outcomes = dataset.read_gwas(gwas_row.path,
                                            gwas_row.effect_type,
                                            filter_regions=[gene_coords[0:3]],
                                            #filter_pvalue = filter_pvalue,
                                            #and_filter=True,
                                            drop_reserved = True,
                                            drop_unusable_data = True,
                                            **read_params)
                #in case of gtex, select the gene we actually wanna consider
                if 'ensembl_gene_id' in outcomes.columns:
                    outcomes = outcomes[outcomes.ensembl_gene_id == gene_coords[-1]]                
                #test_out.append(outcomes)
            except merit_errors.NoDataError:
                with portalocker.Lock(os.path.join(os.path.split(args.outfile)[0],
                                                   'erroneous_gwas_parameter.txt'),
                                      timeout=360,mode='a') as fh:
                    fh.write('- The following Gene-GWAS combination has no suitable variants.\
                             \t{}\t{}\n'.format(
                        gene_coords[-1],gwas_row.path))
                continue
                #in case the read_gwas was not successful because no instruments were able
                #to be selected from that region.
            except Exception as e:
                #in case the read_gwas was not successful due to wrong parameters
                with portalocker.Lock(os.path.join(os.path.split(args.outfile)[0],
                                                   'erroneous_gwas_parameter.txt'),
                                      timeout=360,mode='a') as fh:
                    fh.write('- The following GWAS combination can not be read in.\t{}\t{}\
                             \t{}\n'.format(gene_coords[-1],gwas_row.path,e))
                break

            #!!!As soon as pvalue_filter works, this
            #function can be removed.
            #manually filters pvalue
            outcomes = _filterpvalue(outcomes,gwas_row,args.pvalue)

            #!!! implement maf filter here

            #here I am storing the overlap with gwas id and gene id as a multi index.
            output_dict[(i,gene_coords[-1])] = [outcomes[
                (outcomes.chr_name==gene_coords[0]) &
                (outcomes.start_pos>=gene_coords[1]) &
                (outcomes.end_pos<=gene_coords[2])].index.values]

    #convert dictionary with tuples as keys to dataframe
    overlap_df = pd.DataFrame.from_dict(output_dict,orient='index',columns=['uni_ids'])
    #convert index to multiindex
    overlap_df.index = pd.MultiIndex.from_tuples([(k[0],k[1]) for k in overlap_df.index])

    with portalocker.Lock(args.outfile, timeout=360,mode='ab') as fh:
        pickle.dump(overlap_df,fh)

###################################################################################################

def _test_default_set_args(description=\
        'help function to set args for testing purposes.',
        **kwargs):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description :   str, optional
    **kwargs    :   optional

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description, **kwargs)
    parser.add_argument('--gwas_sets',
                    type=str,
                    help='Path to file with paths to GWAS data sets and their labels for genomic \
                        variations & pvalue. This file needs to be build up similarly to the\
                        outcome files but requires only information about name, path to file\
                        and genomic location.',
                    default='/Users/christiangross/Documents/scripts/gene-gwas-overlap/test_outcomes.tsv'
                    )
    parser.add_argument('--outfile',
                    type=str,
                    help='Path to file which eventually will be returned as a multiindexed\
                        dataframe.',
                    default='/Users/christiangross/Documents/scripts/gene-gwas-overlap/multiindexed_overlap.txt'
                    )
    parser.add_argument('--gene_file',
                    type=str,
                    help='path to a file with the target genes, one per row.',
                    default='/Users/christiangross/Documents/scripts/gene-gwas-overlap/test_genes.txt'
                    )
    parser.add_argument('--gwas_name',
                    type=str,
                    help='The name or names of the GWAS that need to be checked. This can be a\
                        single name or a semicolon seperated list. Alternatively set to None\
                        to select all GWAS present in the gwas_sets file.',
                    default='None'
                    )
    parser.add_argument('--build',
                    type=str,
                    help='Sets assembly version to retrieve gene locations from ensembl\
                            defaults: 37',
                    default='37'
                    )
    parser.add_argument('--up',
                    type=int,
                    help='Upstream flank in bp (int), default: 1000000.',
                    default=1000000)
    parser.add_argument('--down',
                    type=int,
                    help='Downstream flank in bp (int), default: 1000000.',
                    default=1000000)
    parser.add_argument('--pvalue',
                    type=int,
                    help='The -log10 p-value cut-off to select instruments. (int). \
                    Default = 4.',
                    default=4)
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description=\
        'Set parameters',
        **kwargs):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description :   str, optional
    **kwargs    :   optional

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description, **kwargs)
    parser.add_argument('--gwas_sets',
                    type=str,
                    help='Path to file with paths to GWAS data sets and their labels for genomic \
                        variations & pvalue. This file needs to be build up similarly to the\
                        outcome files but requires only information about name, path to file\
                        and genomic location.',
                    default='~/gene-gwas-overlap/test_outcomes.txt'
                    )
    parser.add_argument('--outfile',
                    type=str,
                    help='Path to file which eventually will be returned as a multiindexed\
                        dataframe.',
                    default='~/gene-gwas-overlap/multiindexed_overlap.txt'
                    )
    parser.add_argument('--gene_file',
                    type=str,
                    help='path to a file with the target genes, one per row.',
                    default='~/gene-gwas-overlap/test_genes.txt'
                    )
    parser.add_argument('--gwas_name',
                    type=str,
                    help='The name or names of the GWAS that need to be checked. This can be a\
                        single name or a semicolon seperated list. Alternatively set to None\
                        to select all GWAS present in the gwas_sets file.',
                    default='None'
                    )
    parser.add_argument('--build',
                    type=str,
                    help='Sets assembly version to retrieve gene locations from ensembl\
                            defaults: 37',
                    default='37'
                    )
    parser.add_argument('--up',
                    type=int,
                    help='Upstream flank in bp (int), default: 1000000.',
                    default=1000000)
    parser.add_argument('--down',
                    type=int,
                    help='Downstream flank in bp (int), default: 1000000.',
                    default=1000000)
    parser.add_argument('--pvalue',
                    type=int,
                    help='The -log10 p-value cut-off to select instruments. (int). \
                    Default = 4.',
                    default=4)
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it

    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _select_server_species(assembly):
    """
    Helper function to select the right server url depending on the requested genome assembly.

    Parameters
    ----------
    assembly : str,int,float
        String, int or float, indicating which Human Genome assembly to use, valid values are 37\
            for GRCh37 or 38 for GRCh38.

    Returns
    -------
    server : str
        server url.
    species : str
        species name.

    """
    if isinstance(assembly,(int,float)):
        assembly = str(abs(int(assembly)))

    if assembly == '37':
        server = "https://grch37.rest.ensembl.org"
    elif assembly == '38':
        server="https://rest.ensembl.org"
    else:
        sys.exit('Non supported Genome assembly was requested. {}'.format(assembly))

    #!!!Currently we support only human, in future, if any other species should be made available
    #The species parameter needs to be parseable and server selection needs to be more elaborate.
    species="homo_sapiens"
    return server,species

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_genes_from_file(gene_file):
    """
    This function searches within the tab delimited gene file all human gene Ensembl Identifiers.

    Parameters
    ----------
    gene_file : str
        Path to file containing the human gene Ensembl identifiers. In case of multiple columns,
        they need to be tab delimited and the gene identifiers need to occupy a column on their own

    Returns
    -------
    ids : list of str
        List object containing all human gene Ensembl Identifiers that were found in the file.

    """
    #retrieve all ensembl human gene identifiers
    ids = []
    for lines in open(gene_file):
        line = lines.strip().split('\t')
        #check each element in the gene file for being an ensembl human gene identifier.
        for element in line:
            if element.startswith('ENSG0'):
                ids.append(element)

    if len(ids) == 0:
        sys.exit('No Ensembl Gene Identifiers were able to be retrieved from the provided data\
                 file ({})'.format(gene_file))
    return ids

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def select_gwas(gwas_df,gwas_name,outfile):
    """
    Selects GWAS, from the overal GWAS set, that will be checked against the provided gene set.

    Parameters
    ----------
    gwas_df : pd.DataFrame
        Set of GWAs and their details which can be checked against the set of genes.
    gwas_name : str
        semicolon delimited string containing the names of the GWAS indexes that are supposed to be
        selected for this analysis. Alternatively, False or None can be provided as a string which
        will select all GWAS given in the GWAS set.

    Returns
    -------
    pd.DataFrame
        Containing the set of GWAS that will be checked against the genes.
    """
    gwas_names = [x.strip() for x in gwas_name.split(';')]
    if len(gwas_names) == 1:
        #if gwas name is specified, use all in the file.
        if gwas_names[0].lower() in ['none','false','nan','']:
            return gwas_df

        return gwas_df.loc[gwas_names]

    #returns all the names that are not in the gwas_set.
    not_in_set = gwas_df[~gwas_df.index.isin(gwas_names)].index.to_list()
    if len(not_in_set) != 0:
        with portalocker.Lock(os.path.join(os.path.split(outfile)[0],
                                           'erroneous_gwas_parameter.txt'),
                                           timeout=360,mode='a') as fh:
            fh.write('- The following GWAS name is not in the provided gwas_sets.\t{}\n'.format(
                ';'.join(not_in_set)))

    #exits script in case no gwas can be selected
    if len(not_in_set) == gwas_df.shape[0]:
        sys.exit('No GWAS data can be selected. Please check the indexes of your GWAS set file\
                  and the list of GWAS names. You will find further information in \
                  erroneous_gwas_parameter.txt')

    return gwas_df.loc[gwas_names]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _filterpvalue(df,gwasrow,pvalue):
    """
    This is a temporary function to manually select rows with fitting pvalues, until the pvalue
    filter in merit.dataset.read_gwas works as intended.
    """
    if (gwasrow.pvalue_logged == True) or (gwasrow.pvalue_logged == 'True') or\
    (np.any(df.pvalue > 1)):
        return df[df.pvalue > pvalue]

    return df[df.pvalue < 10**-pvalue]

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
