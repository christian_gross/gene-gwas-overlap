#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 11:47:12 2021

@author: christiangross

TODO:   Write a function which plots the distribution of variations with respect to their genomic
        location.
"""
import os
import argparse
import _pickle as cPickle
import numpy as np
import pandas as pd
import seaborn as sns
import warnings
warnings.simplefilter("ignore")

###################################################################################################
def _test_default_set_args(description=\
        'help function to set args for testing purposes.',
        **kwargs):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description :   str, optional
    **kwargs    :   optional

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description, **kwargs)
    parser.add_argument('--infile',
                    type=str,
                    help='Path to file with paths to GWAS data sets and their labels for genomic \
                        variations & pvalue. This file needs to be build up similarly to the\
                        outcome files but requires only information about name, path to file\
                        and genomic location.',
                    default='/Users/christiangross/Documents/data/gene_gwas_overlap/1e-4/all_multiindexed_results_1e-4.p')

    parser.add_argument('--mapper',
                    type=str,
                    help='Path to mapper file used to convert ensembl ids to gene names. The file\
                        needs two tab seperated columns, with the column header `uniq_id` for the\
                        ensembl ids and `name` for the gene names.',
                    default='/Users/christiangross/Documents/scripts/gene-gwas-overlap/test_genes.txt'
                    )

    parser.add_argument('--exposures',
                    type=str,
                    help='A semicolon seperated list of GWAS identifiers that are supposed to be\
                        exposures for Gene-Exposure-GWAS overlap plots. If not set, only \
                        Gene-GWAS overlap plots are generated, otherwise one plot per exposure is\
                        generated. default is None',
                    default='dbp_pa_ice;sbp_pa_ice'
                    )

    parser.add_argument('--printed_gwas',
                    type=str,
                    help='A semicolon seperated list of GWAS identifiers that indicate which\
                        GWAS are plotted in the heatmap. If None, all available GWAs will \
                        be plotted. default is None',
                    default='dbp_pa_ice;sbp_pa_ice'
                    )

    parser.add_argument('--annotate_counts',
                    type=bool,
                    help='This boolean parameter indicates if the heatmap cells will contain\
                        count numbers.',
                    default=True
                    )

    parser.add_argument('--plottitle',
                    type=str,
                    help='Title of the plot. default is \'\'',
                    default=''
                    )

    parser.add_argument('--outpath',
                    type=str,
                    help='Path to directory in which the plots shall be stored. default is ./',
                    default='./'
                    )

    parser.add_argument('--filename',
                    type=str,
                    help='Name of the file containing the heatmap, If the file already exists, a\
                        numerical prefix will be added. default is plot.png',
                    default=''
                    )
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description=\
        'Set parameters',
        **kwargs):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description :   str, optional
    **kwargs    :   optional

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description, **kwargs)
    parser.add_argument('--infile',
                    type=str,
                    help='Path to file with paths to GWAS data sets and their labels for genomic \
                        variations & pvalue. This file needs to be build up similarly to the\
                        outcome files but requires only information about name, path to file\
                        and genomic location.',
                    default=''
                    )
    parser.add_argument('--mapper',
                    type=str,
                    help='Path to mapper file used to convert ensembl ids to gene names. The file\
                        needs two tab seperated columns, with the column header `uniq_id` for the\
                        ensembl ids and `name` for the gene names.',
                    default=''
                    )
    parser.add_argument('--exposures',
                    type=str,
                    help='Semicolon seperated list of GWAS identifiers that are supposed to be\
                        exposures for Gene-Exposure-GWAS overlap plots. If not set, only \
                        Gene-GWAS overlap plots are generated, otherwise one  default is None',
                    default='None'
                    )
    parser.add_argument('--printed_gwas',
                    type=str,
                    help='A semicolon seperated list of GWAS identifiers that indicate which\
                        GWAS are plotted in the heatmap. If None, all available GWAs will \
                        be plotted.',
                    default='None'
                    )
    parser.add_argument('--annotate_counts',
                    action = 'store_true',
                    help='This boolean parameter indicates if the heatmap cells will contain\
                        count numbers. default = True'
                    )
    parser.add_argument('--plottitle',
                    type=str,
                    help='Title of the plot. default is \'\'',
                    default=''
                    )
    parser.add_argument('--outpath',
                    type=str,
                    help='Path to directory in which the plots shall be stored. default is ./',
                    default='./'
                    )
    parser.add_argument('--filename',
                    type=str,
                    help='Name of the file containing the heatmap, If the file already exists, a\
                        numerical prefix will be added. default is plot.png',
                    default='plot.png'
                    )
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it

    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_data_from_pickle(pickle_file):
    """
    Function to iterate over pickle file entries and concatenates them to one pd.DataFrame.

    Parameters
    ----------
    filename : str
        Filepath to pickle file containing the overlapping variants in a multiindexed data frame.

    Returns
    -------
    pandas.DataFrame
        DESCRIPTION.

    """
    with open(pickle_file,mode='rb') as fh:
        sequential_output = []
        while True:
            try:
                sequential_output.append(cPickle.load(fh))
            except:
                break

    return pd.concat(sequential_output)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _group_reshape(df,gwas_name):
    """
    This is a helper function which renames the dataframe thus it can be reshaped in a immediate
    subsequent step.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame of the selected subset.
    gwas_name : str
        Index of the GWAS.

    Returns
    -------
    df : pandas.DataFrame
        renamed DataFrame which enables the reshaping.

    """
    del df['level_0']
    df.rename(columns = {'uni_ids': gwas_name}, inplace = True)
    return df

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _count_intersect(arr, target_instruments = np.array([])):
    """
    helper function to find the intersection between exposure instrument set and other gwas
    instrument sets.

    Parameters
    ----------
    arr : numpy.ndarray
        numpy array, containing the list of unique variant identifiers for a given gene-gwas.
    target_instruments : numpy.ndarray, optional
        numpy array containing the list of unique variants identifiers of a different gwas but\
        same gene as for arr.
        The default is an empty np.array([]).

    Returns
    -------
    int
        Number of overlapping instruments, In case of NaN values, return 0.
    """
    if isinstance(arr,np.ndarray):
        return len(np.intersect1d(arr,target_instruments))
    #In case of NaN values
    return 0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def rename_ensembl_name(count_df,mapper_file):
    """
    This function renames columns/indexes and transposes the input dataframe.

    Parameters
    ----------
    count_df : pandas.DataFrame
        DataFrame displaying the number of overlapping instruments for any given gene-gwas
        pair.
    mapper_file : str
        Path to file that contains the information used to map ensembl ids to gene names.
        The faile needs to contain at least two columns labelled `uniq_id` for ensembl
        ids and `name` for gene names.

    Returns
    -------
    df : pandas.DataFrame
        The returned dataframe is the transposed count_df with ensembl ids replaced by gene names.
    """

    #taking the transpose and renaming the Ensembl Gene Identifiers by their Gene names.
    df = count_df.transpose().astype(float)
    df[df==0.0]=float('nan')

    mapper = pd.read_csv(mapper_file,sep='\t')
    mapper.set_index('uniq_id',inplace=True)
    new_index = []
    for ind in df.index:
        new_index.append(mapper.loc[ind]['name'])
        
    df.index = new_index  

    return df

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _crop_values(x):
    """
    Help function which crops data values for plotting.

    Parameters
    ----------
    x : str,int
        Cell values that shall be cropped.

    Returns
    -------
    x : str, int
        Cropped value.

    """
    if x == '-':
        return x

    try:
        return round(x,1)
    except TypeError:
        return x

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _name_file(outpath,filename):
    '''
    This function checks if the filename for the plot already exists, if yes it adds an numerical
    prefix or increments an already existing numeral.

    Parameters
    ----------
    outpath : str
        Path to the directory in which the plots are stored.
    filename : str
        Name of the plot file.

    Returns
    -------
    filename : str
        New name of the plot file.
    '''
    #check repeatedly until path+filename does not already exist.
    while os.path.isfile(os.path.join(outpath,filename)):
        try:
            name_elem = filename.split('_')
            prefix = name_elem[0]
            prefix = int(prefix)
            filename = str(prefix+1)+'_'+'_'.join(name_elem[1:])
        except ValueError:
            filename = '1_'+filename
    
    return filename

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def plot_counts_heatmap(df,title='',annotate_counts=False,outpath='./',filename='plot.png'):
    '''
    This function plots the heatmap and saves them to disk.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame containing the counts of overlapping variants.
    title : str, optional
        Title that should be given to the plot. The default is ''.
    annotate_counts : bool, optional
        If set to True, the individual cells will be annotated with their count number. The
        default is False.
    outpath : str, optional
        Path to the directory in which the plots should be stored. The default is './'.
    filename : str, optional
        Name of the plot file. If plotfile already exists, it will get a numeric prefix. If suffix
        is set to .svg, a vector graphic is generated. The default is 'plot.png'.

    Returns
    -------
    None
    '''
    if annotate_counts:
        df2 = df
        df2.fillna(0,inplace=True)
        df2 = df2.astype(int)
        for i,j, in df.iteritems():
            df2[i].loc[(j==0)] = "-"
        df2 = df2.applymap(_crop_values)
        df2 = df2.astype(str)
    else:
        df2 = None

    #I need a non diverging colour palette.because I represent counts.
    col = sns.color_palette("viridis", as_cmap=True)
    sns.set(font_scale=1.2)  # font size
    sns.set_context("paper")

    lims = [df.min().min(), df.max().max()]

    cm = sns.clustermap(df, fmt=".3", linewidths=1.0,
                        figsize=(70 * 0.393700787, 15 * 0.5),
                        cmap=col,
                        annot=df2,
                        cbar_pos=(0.13, 0.68, 0.03, 0.12),
                        annot_kws={"size": 9},
                        center= (lims[1]+lims[0])/2,
                        col_cluster=False,
                        row_cluster=False,
                        vmin=lims[0],
                        vmax=lims[1],
                        xticklabels=True,
                        yticklabels=True,
                       )
    clab=r""
    clabfs=11
    clabpos='left'
    clabtsize=9
    xticklabsize=11
    yticklabsize=11
    #adds label to the colorbar
    cm.ax_cbar.axes.yaxis.set_label_text(clab, fontsize=clabfs)
    #sets the position of the legend label
    cm.ax_cbar.axes.yaxis.set_label_position(clabpos)
    #sets the scale of the colorbar
    cm.ax_cbar.tick_params(labelsize=clabtsize)
    cm.ax_heatmap.set_xticklabels(cm.ax_heatmap.get_xmajorticklabels(),
                                   fontsize = xticklabsize)
    cm.ax_heatmap.set_yticklabels(cm.ax_heatmap.get_ymajorticklabels(),
                                   fontsize = yticklabsize)

    cm.fig.suptitle(title)
    cm.fig.savefig(os.path.join(outpath,_name_file(outpath,filename)))
    cm.fig.clf()
###################################################################################################

def main():
    """
    The main entry point for the script
    """

    ###  getting arguments
    #parser = _test_default_set_args()
    parser = set_args()
    args = parse_args(parser)

    #retrieve data
    output_df = retrieve_data_from_pickle(args.infile)

    #overlap gene-gwas
    if args.exposures.lower() == 'none':

        #Compute the number of instruments for a given Gene-GWAS pair.
        num_ins_df = output_df['uni_ids'].apply(len)

        #by unstacking the multiindexed series, I can create a dataframe that is ready for plotting
        num_ins_df = num_ins_df.unstack(level=-1)

        #renaming the ensembl ids with gene names, transposing dataframe
        df1 = rename_ensembl_name(count_df=num_ins_df,mapper_file = args.mapper)

        #Plot overlap between genes and all available GWAS
        if args.printed_gwas.lower() == 'none':
            plot_counts_heatmap(df1,
                                annotate_counts = args.annotate_counts,
                                outpath = args.outpath,
                                title = args.plottitle,
                                filename = args.filename)
        else: #Plot overlap between genes and selected GWAS
            plot_counts_heatmap(df1[[x.strip() for x in args.printed_gwas.split(';')]],
                                annotate_counts = args.annotate_counts,
                                outpath = args.outpath,
                                title = args.plottitle,
                                filename = args.filename)

    else:
        #overlap gene-exposure-gwas
        reshaped_output_df = output_df.reset_index([0])

        #transforming the dataframe, thus the second level (GWAS index) becomes row index and the
        #first level (Gene identifier) becomes column index
        reshaped_output_df = pd.concat(
                            [
                             _group_reshape(
                                reshaped_output_df.groupby(
                                    reshaped_output_df['level_0']
                                    ).get_group(x),x) for x in reshaped_output_df['level_0'].unique()
                             ],
                            axis=1).T

        #dict contains pd.DataFrame for the overlap between each set of gwas and genes for each
        #exposure:
        overlap_between_gwas_dict = {}

        #the set of GWAS for which the overlap with other GWAS should be determined
        for exposure in [x.strip() for x in args.exposures.split(';')]:
            #this iterates over all rows (GWAS indexes) and computes the intersection between the GWAS
            #of interest (exposure) and all the other GWAS, for all genes (columns)
            overlap_between_gwas_dict[exposure] = pd.concat(
                [reshaped_output_df[x].apply(
                    _count_intersect,
                    target_instruments = reshaped_output_df[x].loc[exposure])
                    for x in reshaped_output_df.columns],
                axis=1)

        #iterates over exposure GWAS, retrieves their gene-exposure-gwas data and plots it.
        for key in overlap_between_gwas_dict:

            #selecting the gene-exposure-gwas, exposure dataset of interest.
            num_ins_df = overlap_between_gwas_dict[key]

            #renaming the ensembl ids with gene names, transposing dataframe
            df1 = rename_ensembl_name(count_df=num_ins_df,mapper_file = args.mapper)

            #Plot overlap between genes and all available GWAS
            if args.printed_gwas.lower() == 'none':
                plot_counts_heatmap(df1,
                                    annotate_counts = args.annotate_counts,
                                    outpath = args.outpath,
                                    #adding key to title and filename
                                    title = key+' '+args.plottitle,
                                    filename = args.filename)
            else: #Plot overlap between genes and selected GWAS
                plot_counts_heatmap(df1[[x.strip() for x in args.printed_gwas.split(';')]],
                                    annotate_counts = args.annotate_counts,
                                    outpath = args.outpath,
                                    title = key+' '+args.plottitle,
                                    filename = key+'_'+args.filename)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
