# Description

The aim of this project is to provide a set of scripts which enables the user to read in GWAS data and determine the number and location of variants that could potentially serve as instruments for cis-MR studies. The number of cis-instruments per investigated gene-gwas pair are determined and the number of variants that overlap between any two GWAS, given a particular gene-exposure-gwas triplet. 

# Installation

This is just a script repository and no module (yet), thus some modules need to be installed manually if not yet available. 
- Chris Finan's ensembl_rest_client [https://gitlab.com/cfinan/ensembl-rest-client](https://gitlab.com/cfinan/ensembl-rest-client)
- Chris Finan's Merit [https://gitlab.com/cfinan/merit](https://gitlab.com/cfinan/merit)
- WoLpH's portalocker [https://pypi.org/project/portalocker/](https://pypi.org/project/portalocker/)

If one, wishes to run job arrays on Myriad rather than individual jobs, this can be done via
Chris Finan's pyjasub script, available here [https://gitlab.com/cfinan/cluster](https://gitlab.com/cfinan/cluster)

# Examples

To run job arrays on the Myriad compute cluster, this repository already provides you with a config- (`gene_gwas_overlap.cnf`), bash- (`check_gene_gwas_overlap.bash`), and example job array file (`gene_gwas_overlap_jaf.tsv`). You have to adjust them for your own needs, then you can submit your job array via `pyjasub gene_gwas_overlap.cnf`. For individual jobs you can either use the bash script which parses arguments based on their order or directly the python script (`check_gene_gwas_overlap.py`). This repository contains already a few test genes (`test_genes.txt`) and files, containing available GWAS and their readin parameters (`all_b37_outcomes.txt`, `all_outcomes.txt`). The completeness of avilable GWAS in the group folders is not guarenteed and all paths are with respect to the file locations on Myriad. Additional GWAS datasets can easily be added as long as the format remains the same. 

`check_gene_gwas_overlap.py` The output can be written in one file, during the writing process the file will be locked. 

`plot_gene_gwas_overlap.py` is generating heatmaps, with Genes on the y-axis and GWAS on the x-axis. Each cell represents the number of instruments that overlap the queried cis-region and the GWAS dataset in question (e.g. **plot.png**). Annotations can be activated which display the exact number of instruments for a particular cell (e.g. 1_plot.png). Further, plots can be generated that show the overlap of instruments for a given genic-cis region, a particular exposure GWAs and outcome GWAS. These plots are created for each selected exposure data set respectively (e.g. dbp_pa_ice_plot.png and sbp_pa_ice_plot.png). Multiple plots with the same name receive a numerical prefix.

Following, I present a few examples to plot the results. The --infile parameter is the generated output of `check_gene_gwas_overlap.py`. It is a pickle file containing multiple, multiindexed pandas DataFrames with the Gene-Gwas pair as multiindex and a list containing the unique identifier of all variants that passed filtering.

- `python plot_gene_gwas_overlap.py --infile ./all_multiindexed_results.p --mapper test_genes.txt --exposures dbp_pa_ice\;sbp_pa_ice --printed_gwas chd\;hf\;chd_ukb\;gtex_artery_coronary_b37 --annotate_counts --plottitle "test gene-exposure-gwas cell annotations for selected gene-exposure-gwas triplets" --outpath ./ --filename plot.png`

- `python plot_gene_gwas_overlap.py --infile ./all_multiindexed_results.p --mapper ./test_genes.txt --plottitle "test gene-gwas no cell annotations, all avail. gwas" --outpath ./ --filename plot.png`

- `python plot_gene_gwas_overlap.py --infile ./all_multiindexed_results.p --mapper ./test_genes.txt --printed_gwas chd\;hf\;chd_ukb\;gtex_artery_coronary_b37 --annotate_counts --plottitle "test gene-gwas cell annotations for selected gene-gwas pairs" --outpath ./ --filename plot.png`

# Support

For any specific questions, slack me via the drug_dev slack channel (drugdev-team.slack.com). 

# Roadmap

No new functionality will be added anymore. It will be checked if the existing code can supplement the merit.analysis_tools and merit.visualisation modules.  

# Authors

Christian Groß

# Project status

Stalled
